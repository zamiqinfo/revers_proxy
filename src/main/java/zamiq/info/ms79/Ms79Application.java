package zamiq.info.ms79;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Ms79Application {

	public static void main(String[] args) {
		SpringApplication.run(Ms79Application.class, args);
	}

}

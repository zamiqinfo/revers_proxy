package zamiq.info.ms79.config;

import org.modelmapper.ModelMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

@Configuration
public class Config {

    @Bean("mapper")
    @Primary
    public ModelMapper getModelMapper() {
        return new ModelMapper();
    }

}

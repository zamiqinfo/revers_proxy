package zamiq.info.ms79.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import zamiq.info.ms79.services.CounterService;

@RestController
@RequiredArgsConstructor
@RequestMapping("/hello")
public class CounterController {

    private final CounterService counterService;

    @GetMapping()
    public String getCounter(){

        return counterService.getCounter();
    }
}

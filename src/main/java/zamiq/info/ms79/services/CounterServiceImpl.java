package zamiq.info.ms79.services;

import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;
import zamiq.info.ms79.domain.Counter;
import zamiq.info.ms79.repository.CounterRepo;

import java.util.Comparator;
import java.util.NoSuchElementException;

@Service
@RequiredArgsConstructor
public class CounterServiceImpl implements CounterService{

    private final CounterRepo counterRepo;
    private final ModelMapper modelMapper;

    @Override
    public String getCounter() {
        Counter counter = null;
        if(counterRepo.findAll()!=null && counterRepo.findAll().size()!=0){
            counter =counterRepo.findAll().stream().max(Comparator.comparing(Counter::getCounter))
                    .orElseThrow(NoSuchElementException::new);
            counter.setCounter(counter.getCounter()+1);
        }else{
            counter=Counter.builder()
                    .counter(1L).build();
        }

        counterRepo.save(counter);
        return "Hello, the counter is "+counter.getCounter();
    }
}

package zamiq.info.ms79.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import zamiq.info.ms79.domain.Counter;

public interface CounterRepo extends JpaRepository<Counter,Long> {

}
